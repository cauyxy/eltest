import {createRouter, createWebHashHistory} from 'vue-router'


const routes = [
    {
        path: "/",
        redirect: "/Login"
    },
    {
        name: "Login",
        path: "/Login",
        component: () => import("../components/Login")
    },
    {
        name: "MyProject",
        path: "/MyProject",
        component: () => import("../components/MyProject")
    },
    {
        name: "MyAdmin",
        path: "/MyAdmin",
        component: () => import("../components/MyAdmin")
    },
    {
        name: "applyRes",
        path: "/applyRes",
        component: () => import("../components/submodule/applyRes")
    },
    {
        name: "returnRes",
        path: "/returnRes",
        component: () => import("../components/submodule/returnRes")
    },
    {
        name: "updateData",
        path: "/updateData",
        component: () => import("../components/submodule/updateData")
    },
    {
        name: "resourceConfirm",
        path: "/resourceConfirm",
        component: () => import("../components/adminmodule/resourceConfirm")
    },
    {
        name: "dataManage",
        path: "/dataManage",
        component: () => import("../components/adminmodule/dataManage")
    },
    {
        name: "returnConfirm",
        path: "/returnConfirm",
        component: () => import("../components/adminmodule/returnConfirm")
    },
    {
        name: "enterLab",
        path: "/enterLab",
        component: () => import("../components/submodule/enterLab")
    },
    {
        name: "doCheck",
        path: "/doCheck",
        component: () => import("../components/submodule/doCheck")
    },
    {
        name: "doLab",
        path: "/doLab",
        component: () => import("../components/submodule/doLab")
    },
    {
        name: "doAssist",
        path: "/doAssist",
        component: () => import("../components/submodule/doAssist")
    },
    {
        name: "doLog",
        path: "/doLog",
        component: () => import("../components/submodule/doLog")
    },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes
})

export default router
